import React from 'react';
import Item from '../Item/Item';

const ItemBoard = ({items}) => 
    <div className="board">
        {items.map(item => {
            return (
            <Item key={item.id} {...item} />
            )}
        )}
    </div> 

export default ItemBoard;