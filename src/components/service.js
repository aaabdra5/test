import items from './mocks/items';

class BaseService {
    static promiseResponse = (data, interval = 500) =>
	    new Promise(resolve => {
		    setTimeout(() => {
			    resolve(data);
		    }, interval);
	});
}

export class ItemService extends BaseService {
    static getItems() {
        return super.promiseResponse(items)
    }
}