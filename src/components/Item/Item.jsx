import React from 'react';
import Title from '../Title/Title';
import List from '../List/List';
import Description from '../Description/Description';

const Item = props => 
    <div className="item">
        <Title>{props.header}</Title>
        <List list={props.option}></List>
        <Description text={props.text}></Description>
    </div>
    
export default Item;