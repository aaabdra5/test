import React from "react";

const List = ({list}) => 
    <ul className="list">
        {list.map(li => <li>{li}</li>)}
    </ul>


export default List;