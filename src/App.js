import React, {Component} from 'react';
import ItemBoard from './components/ItemBoard/ItemBoard';
import { ItemService } from './components/service';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    }
  }

  async componentDidMount() {
    this.setState({
      data: await ItemService.getItems()
    })
  }

  render() {
    return (
      <ItemBoard items={this.state.data} />
    )
  }
}

export default App;
